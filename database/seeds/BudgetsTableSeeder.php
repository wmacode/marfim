<?php

use Illuminate\Database\Seeder;
use App\Models\Budget;

class BudgetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        truncateTable('budgets', true);
        $this->create('Maria', 'maria@oi.com', 1, 1, 1, 10, 140);
        $this->create('Pedro', 'pedro@oi.com', 2, 2, 1, 6, 200);
        $this->create('Clara', 'clara@oi.com', 3, 3, 1, 3, 450);
        enableForeignKeyCheks();
    }

    private function create($name, $email, $serviceId, $placeId, $measureId, $measureQty, $price)
    {
    	$budget = new Budget();
    	$budget->name = $name;
    	$budget->email = $email;
    	$budget->service_id = $serviceId;
    	$budget->place_id = $placeId;
    	$budget->measure_id = $measureId;
        $budget->measure_qty = $measureQty;
    	$budget->price = $price;
    	$budget->save();
    }
}
