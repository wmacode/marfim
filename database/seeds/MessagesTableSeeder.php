<?php

use Illuminate\Database\Seeder;
use App\Models\Message;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        truncateTable('messages', true);
        factory(Message::class, 10)->create();
        enableForeignKeyCheks();
    }
}
