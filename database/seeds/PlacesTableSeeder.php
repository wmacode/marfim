<?php

use Illuminate\Database\Seeder;
use App\Models\Place;

class PlacesTableSeeder extends Seeder
{

	private $places = [
		['name' => 'Casa', 'type' => 'C'],
		['name' => 'Apartamento', 'type' => 'A'],
		['name' => 'Escritorio', 'type' => 'E']
	];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        truncateTable('places', true);
        foreach ($this->places as $place) {
        	$this->create($place['name'], $place['type']);
        }
        enableForeignKeyCheks();
    }

    private function create($name, $type)
    {
    	$place = new Place;
    	$place->name = $name;
    	$place->type = $type;
    	$place->save();
    }
}
