<?php

use Illuminate\Database\Seeder;
use App\Models\Client;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        truncateTable('clients', true);
		factory(Client::class, 4)->create();
		enableForeignKeyCheks();
    }
}
