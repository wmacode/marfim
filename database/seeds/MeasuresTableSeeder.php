<?php

use Illuminate\Database\Seeder;
use App\Models\Measure;

class MeasuresTableSeeder extends Seeder
{

	private $measures = [
		['name' => 'cômodo', 'type' => 'C'],
		['name' => 'ambiente', 'type' => 'A']
	];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        truncateTable('measures', true);
        foreach ($this->measures as $measure) {
        	$this->create($measure['name'], $measure['type']);
        }
        enableForeignKeyCheks();
    }

    private function create($name, $type)
    {
    	$measure = new Measure;
    	$measure->name = $name;
    	$measure->type = $type;
    	$measure->save();
    }
}
