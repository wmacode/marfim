<?php

use Illuminate\Database\Seeder;
use App\Models\Service;

class ServicesTableSeeder extends Seeder
{

	private $services = [
		['name' => 'Dedetização', 'type' => 'D'],
		['name' => 'Desratização', 'type' => 'R'],
		['name' => 'Descupinização', 'type' => 'C']
	];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        truncateTable('services', true);
        foreach ($this->services as $service) {
        	$this->create($service['name'], $service['type']);
        }
        enableForeignKeyCheks();
    }

    private function create($name, $type)
    {
    	$service = new Service;
    	$service->name = $name;
    	$service->type = $type;
    	$service->save();
    }
}
