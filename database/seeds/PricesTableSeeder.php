<?php

use Illuminate\Database\Seeder;
use App\Models\Price;

class PricesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        truncateTable('prices', true);
        $this->create(1, 1, 100.00, 1, 20);
        $this->create(1, 2, 100.00, 1, 30);
        $this->create(1, 3, 120.00, 2, 40);
        $this->create(2, 1, 120.00, 1, 20);
        $this->create(2, 2, 120.00, 1, 30);
        $this->create(2, 3, 140.00, 2, 20);
        $this->create(3, 1, 140.00, 1, 40);
        $this->create(3, 2, 140.00, 1, 40);
        $this->create(3, 3, 150.00, 2, 60);
        enableForeignKeyCheks();
    }

    private function create($serviceId, $placeId, $basePrice, $measureId, $measurePrice)
    {
    	$price = new Price();
    	$price->service_id = $serviceId;
    	$price->place_id = $placeId;
    	$price->price = $basePrice;
    	$price->measure_id = $measureId;
    	$price->measure_price = $measurePrice;
    	$price->save();
    }
}
