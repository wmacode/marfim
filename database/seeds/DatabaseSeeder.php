<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(MessagesTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(PlacesTableSeeder::class);
        $this->call(MeasuresTableSeeder::class);
        $this->call(PricesTableSeeder::class);
        $this->call(BudgetsTableSeeder::class);
        $this->call(ClientsTableSeeder::class);
    }
}
