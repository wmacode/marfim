<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        truncateTable('users', true);
        factory(User::class)->create([
        	'name' => 'Joao',
        	'email' => 'joao@oi.com',
        	'password' => bcrypt('joao10')
        ]);
        enableForeignKeyCheks();
    }
}
