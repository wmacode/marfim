<?php

use Faker\Generator as Faker;
use App\Models\Client;

$factory->define(Client::class, function (Faker $faker) {
    $filePath = storage_path('app/public/images/clients');
    return [
        'name' => $faker->word,
        'image' => $faker->image($filePath, 230, 230)
    ];
});
