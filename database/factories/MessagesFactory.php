<?php

use Faker\Generator as Faker;
use App\Models\Message;

$faker = \Faker\Factory::create('pt_BR');

$factory->define(Message::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
		'email' => $faker->unique()->freeEmail,
		'subject' => $faker->sentence($nbWords = 6, $variableNbWords = true),
		'message' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true)
    ];
});
