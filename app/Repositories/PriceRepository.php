<?php

namespace App\Repositories;

use App\Models\Price;

class PriceRepository
{

	public function findAll()
	{
		$query = "
			SELECT pr.id, se.name AS service, pl.name AS place, pr.price AS price, me.name AS measure,
				   pr.measure_price AS measurePrice
		      FROM prices pr
		      JOIN services se ON se.id = pr.service_id
		      JOIN places pl ON pl.id = pr.place_id
		      JOIN measures me ON me.id = pr.measure_id
		  ORDER BY pr.id";
		return \DB::select($query);
		
	}

	public function findById($id)
	{
		return Price::findOrFail($id);
	}

	public function update($id, $price, $measurePrice)
	{
		$priceModel = Price::find($id);
		$priceModel->price = $price;
		$priceModel->measure_price = $measurePrice;
		$priceModel->save();
	}

	public function findByServiceAndPlace($serviceId, $placeId)
	{
		$price = Price::where([
			['service_id', $serviceId],
			['place_id', $placeId]
		])->get()->first();
		return $price;
	}
}