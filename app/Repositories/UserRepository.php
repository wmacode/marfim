<?php

namespace App\Repositories;

use App\User;
use App\Services\StorageService;
use DB;

class UserRepository
{
	const IMAGE_STORAGE_PATH_PREFIX_PUBLIC = 'storage/images/users/';

	public function findById($id)
	{
		return User::findOrFail($id);
	}

	public function updateProfilePicture($id, $fileName)
	{
		$user = User::findOrFail($id);
		$oldFileName = $user->image;
		$user->image = $fileName;
		$user->save();
		StorageService::deleteStoredFile(self::IMAGE_STORAGE_PATH_PREFIX_PUBLIC . '' . $oldFileName);
	}

	public function updatePassword($request)
	{
		extract($request);
		$user = $this->findById($userId);
		if(\Hash::check($currentPassword, $user->password)){
			$user->password = bcrypt($newPassword);
			$user->save();
		} else {
			throw new \Exception('Senha atual inválida!');
		}
	}
}