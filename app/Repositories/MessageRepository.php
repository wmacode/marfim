<?php

namespace App\Repositories;

use App\Models\Message;
use DB;

class MessageRepository
{

	public function findAll()
	{
		return Message::all();
	}

	public function create($request)
	{
		return Message::create($request);
	}
}