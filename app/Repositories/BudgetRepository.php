<?php

namespace App\Repositories;

use App\Models\Budget;
use DB;

class BudgetRepository
{

	public function findAll()
	{
		$query = "
			SELECT bud.id, bud.name, bud.email, ser.name AS service, pla.name AS place,
			       mea.name AS measure, bud.measure_qty, bud.price
			  FROM budgets bud
			  JOIN services ser ON ser.id = bud.service_id
			  JOIN places pla ON pla.id = bud.place_id
			  JOIN measures mea ON mea.id = bud.measure_id
			  ORDER BY bud.id DESC";

		return \DB::select($query);
	}


	public function create($name, $email, $serviceId, $placeId, $measureId, $measureQty, $price)
	{
		$budget = new Budget;
		$budget->name = $name;
		$budget->email = $email;
		$budget->service_id = $serviceId;
		$budget->place_id = $placeId;
		$budget->measure_id = $measureId;
		$budget->measure_qty = $measureQty;
		$budget->price = $price;
		$budget->save();
		return $budget;
	}
}