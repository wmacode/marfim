<?php

namespace App\Repositories;

use App\Models\Client;
use App\Services\StorageService;
use DB;

class ClientRepository
{
	const IMAGE_STORAGE_PATH_PREFIX_PUBLIC = 'storage/images/clients/';

	public function findAll()
	{
		return Client::all();
	}

	public function findById($id)
	{
		return Client::findOrFail($id);
	}

	public function create($request)
	{
		return Client::create($request);
	}

	public function save($name, $pathImage)
	{
		$client = new Client;
		$client->name = $name;
		$client->image = $pathImage;
		$client->save();
	}

	public function delete($id)
	{
		$client = $this->findById($id);
		$isDeleted = StorageService::deleteStoredFile(self::IMAGE_STORAGE_PATH_PREFIX_PUBLIC . '' . $client->image);
		// if($isDeleted){
			$client->delete();
		// } else{
		//	throw new \Exception('Error ao excluir imagem no servidor');
		// }
	}
}