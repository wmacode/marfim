<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

function truncateTable($table, $disableForeignKeyCheks = false){
    if($disableForeignKeyCheks){
        disableForeignKeyCheks();
    }
    DB::table($table)->truncate();
}

function disableForeignKeyCheks(){
    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
}

function enableForeignKeyCheks(){
    DB::statement('SET FOREIGN_KEY_CHECKS=1;');
}

function dropTableInDeveloper($table){
    if(env('APP_ENV') == 'local'){
        enableForeignKeyCheks();
        Schema::dropIfExists($table);
        enableForeignKeyCheks();
    }
}

function getClientImageStorageBaseUrl () {
    if(config('app.env') == 'local'){
        return 'http://127.0.0.1:8000//storage/images/clients/';
    } else {
        return 'http://www.marfimdedetizadora.xyz/storage/images/clients/';
    }
}

function getUserImageStorageBaseUrl () {
    if(config('app.env') == 'local'){
        return 'http://127.0.0.1:8000/storage/images/users/';
    } else {
        return 'http://www.marfimdedetizadora.xyz/storage/images/users/';
    }
}

function getArrayError($message, $e){
    return [
        'error' => [
            'msg' => $message,
            'message' => $e->getMessage(),
            'file' => $e->getFile(),
            'line' => $e->getLine()
        ]
    ];
}

