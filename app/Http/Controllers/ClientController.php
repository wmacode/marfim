<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ClientService;
use App\Services\StorageService;
use App\Repositories\ClientRepository;
use App\Http\Requests\ClientValidation;
use Storage;

class ClientController extends Controller
{
	const IMAGE_STORAGE_PATH_PREFIX = 'public/images/clients/';

    private $clientService;
    private $clientRepository;

    public function __construct(ClientService $clientService, ClientRepository $clientRepository)
    {
    	$this->clientService = $clientService;
    	$this->clientRepository = $clientRepository;
    }

    public function findAll()
    {
    	try{
    		$clients = $this->clientRepository->findAll();
            $clients = StorageService::createImagePath($clients, getClientImageStorageBaseUrl());
    		return response()->json($clients, 200);
    	} catch(\Exception $e){
    		return response()->json(['error' => $e->getMessage()], 500);
    	}
    }

    public function save(ClientValidation $request)
    {
    	try{
            $name = $request->name;
            $fileName = StorageService::fileStorage($request->image, SELF::IMAGE_STORAGE_PATH_PREFIX);
    		$this->clientRepository->save($name, $fileName);
    		return response()->json(['msg' => 'Cliente cadastrado com sucesso'], 200);
    	} catch(\Exception $e){
    		return response()->json(['error' => $e->getMessage()], 500);
    	}
    }

    public function delete(Request $request)
    {
    	try{
    		$this->clientRepository->delete($request->id);
    		return response()->json(['msg' => 'Cliente excluído com sucesso'], 200);
    	} catch(\Exception $e){
    		return response()->json(['error' => $e->getMessage()], 500);
    	}
    }
}
