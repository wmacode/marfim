<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Services\StorageService;
use App\Services\UserService;
use App\Repositories\UserRepository;
use App\Http\Requests\UserPictureValidation;
use App\Http\Requests\UserPasswordValidation;

class UserController extends Controller
{

	const IMAGE_STORAGE_PATH_PREFIX = 'public/images/users/';

	private $userService;
	private $userRepository;

    public function __construct(UserRepository $userRepository, UserService $userService)
    {
    	$this->userRepository = $userRepository;
    	$this->userService = $userService;
    }

    public function findUserAuth()
    {
        $user = Auth::user();
        if($user->image){
        	$user->filePath = getUserImageStorageBaseUrl() . '' . $user->image; 
        }
        return $user;
    }

    public function updateProfilePicture(UserPictureValidation $request)
    {
    	try{
    		$id = $request->id;
		    $fileName = StorageService::fileStorage($request->image, SELF::IMAGE_STORAGE_PATH_PREFIX);
		    $this->userRepository->updateProfilePicture($id, $fileName);
		    return response()->json(['msg' => 'Foto de perfil do usuário editada com sucesso'], 200);
		} catch(\Exception $e){
    		return response()->json(['error' => $e->getMessage()], 500);
    	}
    }

    public function deleteProfilePicture(Request $request)
    {
    	try{
    		$this->userRepository->updateProfilePicture($request->id, '');
		    return response()->json(['msg' => 'Foto de perfil do usuário excluída com sucesso'], 200);
		} catch(\Exception $e){
    		return response()->json(['error' => $e->getMessage()], 500);
    	}
    }

    public function updatePassword(UserPasswordValidation $request)
    {
    	try{
    		$errorValidate = $this->userService->validatePasswordUpdate($request->all());
    		if(!$errorValidate){
    			$this->userRepository->updatePassword($request->all());
		    	return response()->json(['msg' => 'Senha alterada com sucesso'], 200);
    		} else {
    			throw new Exception($errorValidate);
    		}
		} catch(\Exception $e){
    		return response()->json(['error' => $e->getMessage()], 500);
    	}
    }

}
