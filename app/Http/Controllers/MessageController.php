<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\MessageRepository;

class MessageController extends Controller
{

	private $messageRepository;

    public function __construct(MessageRepository $MessageRepository)
    {
    	$this->messageRepository = $MessageRepository;
    }

    public function findAll()
    {
    	try{
    		$messages = $this->messageRepository->findAll();
    		return response()->json($messages, 200);
    	} catch(\Exception $e){
    		return response()->json([
                'errors' => 'Não foi possivel buscar mensagens.'
            ], 500);
    	}
    }
}
