<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;
use App\Http\Requests\ServiceCreateValidation;
use App\Http\Requests\ServiceUpdateValidation;
use \Illuminate\Database\Eloquent\ModelNotFoundException;

class ServiceController extends Controller
{
    public function findAll()
    {
    	try{
	    	$services = Service::all();
	    	return response()->json($services, 200);
	    } catch(\Exception $e){
    		return response()->json([
                getArrayError('Não foi possivel buscar serviços.', $e)
            ], 500);
    	}
    }

    public function find($id)
    {
    	try{
	    	$service = Service::find($id);
	    	return response()->json($service, 200);
	    } catch(\Exception $e){
    		return response()->json([
                getArrayError('Não foi possivel encontrar o serviço.', $e)
            ], 500);
    	}
    }

    public function create(ServiceCreateValidation $request)
    {
    	try{
	    	Service::create($request->all());
	    	return response()->json(['message' => 'Serviço criado com sucesso!!!'], 201);
	    } catch(ModelNotFoundException $e){
    		return response()->json([
                getArrayError('Não foi possivel encontrar o serviço.', $e)
            ], 200);
    	}
    }

    public function update(ServiceUpdateValidation $request)
    {
        try{
            $service = Service::findOrFail($request->id);
            $service->name = $request->name;
            $service->type = $request->type;
            $service->save();
            return response()->json(['message' => 'Serviço alterado com sucesso!'], 200);
        } catch(ModelNotFoundException $e){
            return response()->json(getArrayError('Não foi possível editar o serviço.', $e), 200);
        }
    }

    public function delete($id)
    {
        try{
            $service = Service::findOrFail($id);
            $service->delete();
            return response()->json(['message' => 'Serviço excluído com sucesso!'], 200);
        } catch(ModelNotFoundException $e){
            return response()->json(getArrayError('Não foi possível excluir o serviço.', $e), 200);
        }
    }
}

