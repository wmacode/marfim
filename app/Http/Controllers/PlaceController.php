<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Place;
use App\Http\Requests\PlaceCreateValidation;
use App\Http\Requests\PlaceUpdateValidation;

class PlaceController extends Controller
{
    public function findAll()
    {
    	try{
	    	$place = Place::all();
	    	return response()->json($place, 200);
	    } catch(\Exception $e){
    		return response()->json([
                getArrayError('Não foi possivel encontrar locais.', $e)
            ], 500);
    	}
    }

    public function find($id)
    {
    	try{
	    	$place = Place::find($id);
	    	return response()->json($place, 200);
	    } catch(\Exception $e){
    		return response()->json([
                getArrayError('Não foi possivel encontrar o local.', $e)
            ], 500);
    	}
    }

    public function create(PlaceCreateValidation $request)
    {
    	try{
	    	Place::create($request->all());
	    	return response()->json(['message' => 'Local criado com sucesso!!!'], 201);
	    } catch(ModelNotFoundException $e){
    		return response()->json([
                getArrayError('Não foi possivel encontrar o local.', $e)
            ], 200);
    	}
    }

    public function update(PlaceUpdateValidation $request)
    {
        try{
            $place = Place::findOrFail($request->id);
            $place->name = $request->name;
            $place->type = $request->type;
            $place->save();
            return response()->json(['message' => 'Local alterado com sucesso!'], 200);
        } catch(ModelNotFoundException $e){
            return response()->json(getArrayError('Não foi possível editar o local.', $e), 200);
        }
    }

    public function delete($id)
    {
        try{
            $place = Place::findOrFail($id);
            $place->delete();
            return response()->json(['message' => 'Local excluído com sucesso!'], 200);
        } catch(ModelNotFoundException $e){
            return response()->json(getArrayError('Não foi possível excluir o local.', $e), 200);
        }
    }
}
