<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UpdatePriceValidation;
use App\Services\PriceService;
use App\Repositories\PriceRepository;

class PriceController extends Controller
{

	private $priceService;
	private $priceRepository;

    public function __construct(PriceService $priceService, PriceRepository $priceRepository)
    {
    	$this->priceService = $priceService;
    	$this->priceRepository = $priceRepository;
    }

    public function findAll()
    {
    	try{
	    	$prices = $this->priceRepository->findAll();
	    	return response()->json($prices, 200);
	    } catch(\Exception $e){
    		return response()->json([
                'errors' => 'Não foi possivel buscar orçamentos.'
            ], 500);
    	}
    }

    public function update(UpdatePriceValidation $request)
    {
    	try{
	    	extract($request->all());
	    	$this->priceRepository->update($id, $price, $measurePrice);
	    	return response()->json(['msg' => 'Preço alterado com sucesso'], 200);
	    } catch(\Exception $e){
    		return response()->json([
                'errors' => 'Não foi possivel buscar orçamentos.'
            ], 500);
    	}
    }
}
