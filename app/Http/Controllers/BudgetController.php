<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\BudgetRepository;

class BudgetController extends Controller
{

	protected $budgetRepository;

	public function __construct(BudgetRepository $budgetRepository)
	{
		$this->budgetRepository = $budgetRepository;
	}

    public function findAll()
    {
    	try{
	    	$budgets = $this->budgetRepository->findAll();
	    	return response()->json($budgets, 200);
	    } catch(\Exception $e){
    		return response()->json([
                'errors' => 'Não foi possivel buscar orçamentos.'
            ], 500);
    	}
    }
}
