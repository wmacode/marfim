<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Requests\SendBudgetValidation;
use App\Http\Requests\SendMessageValidation;
use App\Models\Message;
use App\Models\Service;
use App\Models\Place;
use App\Repositories\MessageRepository;
use App\Repositories\ClientRepository;
use App\Services\BudgetService;
use App\Services\MessageService;
use App\Services\StorageService;

class SiteController extends Controller
{
    const SUC_MSG_BUDGET = 'Orçamento enviado com sucesso! Em breve enviaremos a resposta para seu email.';

    private $budgetService;
    private $messageService;
    private $messageRepository;
    private $clientRepository;

    public function __construct(BudgetService $budgetService, MessageRepository $messageRepository, MessageService $messageService, ClientRepository $clientRepository)
    {
        $this->budgetService = $budgetService;
        $this->messageService = $messageService;
        $this->messageRepository = $messageRepository;
        $this->clientRepository = $clientRepository;
    }

    public function viewIndex()
    {
        $services = Service::all(['id', 'name']);
        $places = Place::all(['id', 'name']);
        $clients = $this->clientRepository->findAll();
        $clients = StorageService::createImagePath($clients, getClientImageStorageBaseUrl());
        return view('site.index', compact('services', 'places', 'clients'));
    }
    
    public function sendBudget(SendBudgetValidation $request)
    {
        // try {
            $this->budgetService->createBudget($request->all());
            return response()->json(['message' => self::SUC_MSG_BUDGET], 201);
        // } catch (\Exception $e) {
            return response()->json([
                'errors' => ['error' => ['Não foi possivel enviar orçamento.']]
            ], 500);
        // }
    }

    public function sendMessage(SendMessageValidation $request)
    {
        try {
            $messageData = $request->all();
            extract($messageData);
            $this->messageService->sendEmail($name, $email, $subject, $message);
            $this->messageRepository->create($messageData);
            return response()->json(['message' => 'Mensagem enviada com sucesso'], 201);
        } catch (\Exception $e) {
            return response()->json([
                'errors' => ['error' => ['Não foi possivel enviar mensagem.']]
            ], 500);
        }
    }
}

