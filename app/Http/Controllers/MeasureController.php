<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Measure;
use App\Http\Requests\MeasureCreateValidation;
use App\Http\Requests\MeasureUpdateValidation;


class MeasureController extends Controller
{
    public function findAll()
    {
    	try{
	    	$measures = Measure::all();
	    	return response()->json($measures, 200);
	    } catch(\Exception $e){
    		return response()->json([
                getArrayError('Não foi possivel encontrar medidas.', $e)
            ], 500);
    	}
    }

    public function find($id)
    {
    	try{
	    	$measure = Measure::find($id);
	    	return response()->json($measure, 200);
	    } catch(\Exception $e){
    		return response()->json([
                getArrayError('Não foi possivel encontrar a medida.', $e)
            ], 500);
    	}
    }

    public function create(MeasureCreateValidation $request)
    {
    	try{
	    	Measure::create($request->all());
	    	return response()->json(['message' => 'Medida criada com sucesso!!!'], 201);
	    } catch(ModelNotFoundException $e){
    		return response()->json([
                getArrayError('Não foi possivel encontrar a medida.', $e)
            ], 200);
    	}
    }

    public function update(MeasureUpdateValidation $request)
    {
        try{
            $measure = Measure::findOrFail($request->id);
            $measure->name = $request->name;
            $measure->type = $request->type;
            $measure->save();
            return response()->json(['message' => 'Medida alterado com sucesso!'], 200);
        } catch(ModelNotFoundException $e){
            return response()->json(getArrayError('Não foi possível editar a medida.', $e), 200);
        }
    }

    public function delete($id)
    {
        try{
            $measure = Measure::findOrFail($id);
            $measure->delete();
            return response()->json(['message' => 'Medida excluída com sucesso!'], 200);
        } catch(ModelNotFoundException $e){
            return response()->json(getArrayError('Não foi possível excluir a medida.', $e), 200);
        }
    }
}
