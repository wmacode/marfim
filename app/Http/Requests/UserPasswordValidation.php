<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserPasswordValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'currentPassword'       => 'required',
            'newPassword'           => 'required | min:6',
            'confirmNewPassword'    => 'required | min:6',
        ];
    }

    public function messages()
    {
        return [
            'currentPassword.required'  => 'O campo senha atual é obrigatório',
            'newPassword.required'  => 'O campo nova senha é obrigatório',
            'newPassword.min'  => 'O campo nova senha não pode ter menos que 6 caracteres',
            'confirmNewPassword.required'  => 'O campo confirme nova senha é obrigatório',
            'confirmNewPassword.min'  => 'O campo confirme nova senha não pode ter menos que 6 caracteres',
        ];
    }
}
