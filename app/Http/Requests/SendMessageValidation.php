<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendMessageValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required | max:255',
            'email'     => 'required | email | max:255',
            'subject'   => 'required | max:100',
            'message'   => 'required | max:500'
        ];
    }

    public function messages()
    {
        return [
            'name.required'     => 'O campo nome é obrigatório.',
            'email.required'    => 'O campo email é obrigatório.',
            'email.email'       => 'Digite um email válido.',
            'subject.required'  => 'O campo assunto é obrigatório.',
            'subject.max'       => 'Assunto grande de mais.',
            'message.required'  => 'Digite sua mensagem.',
            'message.max'       => 'A mensagem deve ter no maximo 500 caractéres.'
        ];
    }
}
