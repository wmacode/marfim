<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePriceValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price'         => 'required',
            'measurePrice'  => 'required'
        ];
    }

    public function messages()
    {
        return [
            'price.required'  => 'Preencha o campo preço.',
            'measurePrice.required'  => 'Preencha o campo preço por medida.'
        ];
    }
}
