<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserPictureValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required | mimes:jpeg,jpg,png',
        ];
    }

    public function messages()
    {
        return [
            'image.required'  => 'O campo imagem é obrigatório',
            'image.mimes'     => 'O tipo da imagem não é permitido'
        ];
    }
}
