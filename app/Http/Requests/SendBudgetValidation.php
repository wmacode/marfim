<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendBudgetValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required | max:255',
            'email'         => 'required | email | max:255',
            'service_id'    => 'required | max:255',
            'place_id'      => 'required | max:255',
            'measure_qty'   => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required'         => 'O campo nome é obrigatório',
            'email.required'        => 'O campo email é obrigatório',
            'email.email'           => 'Digite um email válido',
            'servce.required'       => 'O campo tipo de serviço é obrigatório',
            'place.required'        => 'O campo local de serviço é obrigatório',
            'measure_qty.required'  => 'O campo quantidade é obrigatório'
        ];
    }
}
