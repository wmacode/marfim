<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlaceCreateValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required | max:255',
            'type' => 'required | max:1 | min:1 | alpha',
        ];
    }

    public function messages()
    {
        return [
            'name.required'   => 'O campo nome é obrigatório',
            'name.max'        => 'O campo nome pode ter no máximo 255 caracteres',
            'type.required'   => 'O campo tipo é obrigatório',
            'type.max'        => 'O campo tipo tem que ter 1 caracteres',
            'type.alpha'      => 'O campo tipo deve conter apenas letras'
        ];
    }
}
