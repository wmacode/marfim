<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required | max:255',
            'image' => 'required | mimes:jpeg,jpg,png',
        ];
    }

    public function messages()
    {
        return [
            'name.required'   => 'O campo nome é obrigatório',
            'name.max'        => 'O campo nome pode ter no máximo 255 caracteres',
            'image.required'  => 'O campo imagem é obrigatório',
            'image.mimes'     => 'O tipo da imagem não é permitido'
        ];
    }
}
