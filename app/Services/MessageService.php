<?php

namespace App\Services;

use App\Mail\MessageMail;
use Mail;

class MessageService
{

	public function sendEmail($name, $email, $subject, $message)
	{
		$mailData = new \StdClass;
		$mailData->name = $name;
		$mailData->email = $email;
		$mailData->subject = $subject;
		$mailData->message = $message;
		Mail::to('contato@marfimdedetizadora.xyz')->send(new MessageMail($mailData));
	}

}