<?php

namespace App\Services;

use Storage;

class StorageService
{
	public static function fileStorage($file, $prefixStoragePath)
	{
		$fileName = md5(str_random(16)) . "" . $file->getClientOriginalName();
    	$file->storeAs($prefixStoragePath, $fileName);
    	return $fileName;
	}

	public static function deleteStoredFile($filePath)
	{
		return \File::delete(public_path($filePath));
	}

	public static function createImagePath($data, $baseStoragePath)
	{
		foreach ($data as $elem) {
    		$elem->filePath = $baseStoragePath .''. $elem->image;
		}
		return $data;
	}
}