<?php

namespace App\Services;

use App\Repositories\UserRepository;


class UserService
{
	protected $userRepository;

	public function __construct(UserRepository $userRepository)
	{
		$this->userRepository = $userRepository;
	}

	public function validatePasswordUpdate($request)
	{
		extract($request);

      	if($newPassword != $confirmNewPassword){
			return 'Novas senhas não conferem';
		}
    
      	if($currentPassword == $newPassword){
			return 'A senha nova não pode ser igual a senha atual';
		}
		return false;
	}
}