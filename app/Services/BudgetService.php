<?php

namespace App\Services;

use App\Models\Measure;
use App\Repositories\PriceRepository;
use App\Repositories\BudgetRepository;
use App\Notifications\BudgetResponse;
use Notification;

class BudgetService
{

	protected $priceRepository;
	protected $budgetRepository;

	public function __construct(PriceRepository $priceRepository, BudgetRepository $budgetRepository)
	{
		$this->priceRepository = $priceRepository;
		$this->budgetRepository = $budgetRepository;
	}

	public function createBudget($request)
	{
		extract($request);
		$price = $this->priceRepository->findByServiceAndPlace($service_id, $place_id);
		$budgetPrice = $price->price + ($price->measure_price * $measure_qty);
		$budget = $this->budgetRepository->create($name, $email, $service_id, $place_id, $price->measure_id, $measure_qty, $budgetPrice);
		Notification::send($budget, new BudgetResponse($budget));

	}
}