<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
	protected $fillable = ['name', 'type'];
    protected $hidden = ['created_at', 'updated_at'];

    public function prices()
    {
        return $this->hasMany('App\Models\Price');
    }

    public function budgets()
    {
        return $this->hasMany('App\Models\Budget');
    }
}
