<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{

    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }

    public function place()
    {
        return $this->belongsTo('App\Models\Place');
    }

    public function measure()
    {
        return $this->belongsTo('App\Models\Measure');
    }
}
