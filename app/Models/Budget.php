<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Budget extends Model
{
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'service_id', 'place_id', 'measure_id', 'measure_qty', 'price'
    ];
    
    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }

    public function place()
    {
        return $this->belongsTo('App\Models\Place');
    }

    public function measure()
    {
        return $this->belongsTo('App\Models\Measure');
    }
}
