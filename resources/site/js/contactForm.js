var self = this;

$("#submit").click(function(e) {
  var name = $("#name").val();
  var email = $("#email").val();
  var subject = $("#subject").val();
  var message = $("#message").val();
  var response = self.validateData(name, email, subject, message);
  if (response) {
    var data = { name: name, email: email, subject: subject, message: message };
    self.sendData(data);
  }
});

this.sendData = function(data) {
  $(".loader").css("display", "block");
  $("#submit").addClass("disabled");
  $.ajax({
    url: "http://www.marfimdedetizadora.xyz/contato",
    type: "POST",
    data: data,
    headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
    dataType: "json"
  })
    .done(function(response) {
      $(".loader").css("display", "none");
      $("#submit").removeClass("disabled");
      self.clearFields();
      self.msgSuccess(response.message);
    })
    .fail(function(jqXHR) {
      $(".loader").css("display", "none");
      $("#submit").removeClass("disabled");
      var errors = jqXHR.responseJSON.errors;
      var msgErro = "";
      $.each(errors, function(index, value) {
        msgErro += value[0] + "</br>";
      });
      self.msgErro(msgErro);
    });
};

this.msgSuccess = function(msgSuccess) {
  swal("OK", msgSuccess, "success");
};

this.msgErro = function(msgErro) {
  swal("Erro", msgErro, "error");
  return false;
};

this.validateData = function(name, email, subject, message) {
  if (name == "") {
    return self.msgErro("Campo nome é obrigatório.");
  }
  if (name.length < 3) {
    return self.msgErro("Digite um nome válido.");
  }
  if (email == "") {
    return self.msgErro("Campo email é obrigatório.");
  }
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if (!regex.test(email)) {
    return self.msgErro("Digite um email válido.");
  }

  if (subject == "" || subject.length < 4) {
    return self.msgErro("Digite um assunto válido.");
  }

  if (message == "" || message.length < 10) {
    return self.msgErro("Digite uma mensagem válida.");
  }
  return true;
};

this.clearFields = function() {
  $("#name").val("");
  $("#email").val("");
  $("#subject").val("");
  $("#message").val("");
};
