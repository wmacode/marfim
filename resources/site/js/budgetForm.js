var self = this;

$("#submitBudget").click(function(e) {
  var name = $("#formBudgetName").val();
  var email = $("#formBudgetEmail").val();
  var service_id = $("#formBudgetService").val();
  var place_id = $("#formBudgetLocate").val();
  var measure_qty = $("#formBudgetRoom").val();
  var response = self.validateData(
    name,
    email,
    service_id,
    place_id,
    measure_qty
  );
  if (response) {
    var data = {
      name: name,
      email: email,
      service_id: service_id,
      place_id: place_id,
      measure_qty: measure_qty
    };
    self.sendData(data);
  }
  return false;
});

this.sendData = function(data) {
  $(".loader").css("display", "block");
  $("#submitBudget").prop("disabled", true);
  $.ajax({
    url: "http://www.marfimdedetizadora.xyz/orcamento",
    type: "POST",
    data: data,
    headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") },
    dataType: "json"
  })
    .done(function(response) {
      $(".loader").css("display", "none");
      $("#submitBudget").prop("disabled", false);
      console.log(response);
      self.clearFields();
      self.msgSuccess(response.message);
    })
    .fail(function(jqXHR) {
      $(".loader").css("display", "none");
      $("#submitBudget").prop("disabled", false);
      var res = jqXHR.responseJSON;
      var errors = res.errors ? res.errors : res.message;
      var msgErro = "";
      $.each(errors, function(index, value) {
        msgErro += value[0] + "</br>";
      });
      self.msgErro(msgErro);
    });
};

this.msgSuccess = function(msgSuccess) {
  swal("OK", msgSuccess, "success");
};

this.msgErro = function(msgErro) {
  swal("Erro", msgErro, "error");
  return false;
};

this.validateData = function(name, email, service_id, place_id, measure_qty) {
  if (name == "") {
    return self.msgErro("Campo nome é obrigatório.");
  }
  if (name.length < 3) {
    return self.msgErro("Digite um nome válido.");
  }
  if (email == "") {
    return self.msgErro("Campo email é obrigatório.");
  }
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if (!regex.test(email)) {
    return self.msgErro("Digite um email válido.");
  }

  if (!service_id) {
    return self.msgErro("Selecione um servico.");
  }

  if (!place_id) {
    return self.msgErro("Selecione um local.");
  }

  if (measure_qty == "" || measure_qty < 1) {
    return self.msgErro("Digite uma quantidade.");
  }
  return true;
};

this.clearFields = function() {
  $("#formBudgetName").val("");
  $("#formBudgetEmail").val("");
  $("#formBudgetService").val(0);
  $("#formBudgetLocate").val(0);
  $("#formBudgetRoom").val("");
  $("#formBudgetRoom").attr("readonly", true);
  $("#formBudgetRoom").attr("placeholder", "");
};
