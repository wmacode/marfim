<html>
<head>
	<style type="text/css" media="screen">
		body {
			padding: 40px;
			font-family: verdana, arial, sans-serif;
		}
		h3 {
			color: #1f754f;
		}
		p {
			font-size: 1.2em;
		}
	</style>
</head>
<body>
<h3>Mensagem do site</h3>
<p> {{ $mail->message }} </p>
</body>
</html>