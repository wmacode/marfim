<section id="about">
    <div class="container">
        <div class="section-header">
            <h3 class="section-title">Sobre nós</h3>
        </div>
        <div class="row about-container">
            <div class="col-lg-6 content order-lg-1 order-2">
                <h4 class="title">Um pouco de nós</h4>
                <p>
                    Marfim Dedetizadora é uma empresa familiar e está no mercado a mais de 10 anos prestando serviços com qualidade. Temos como
                    característica fidelizar nossos clientes. Através de nossos serviços prestados buscamos a melhor experiencia para nossos
                    clientes.
                </p>
                <div class="icon-box wow fadeInUp">
                    <div class="icon"><i class="fa fa-shopping-bag"></i></div>
                    <h4 class="title"><a href="">Profissionais</a></h4>
                    <p class="description">Contamos com um time de profissionais capacitados e especializados nos tipos de serviços que prestamos.
                    </p>
                </div>
                <div class="icon-box wow fadeInUp" data-wow-delay="0.2s">
                    <div class="icon"><i class="fa fa-photo"></i></div>
                    <h4 class="title"><a href="">Equipamentos</a></h4>
                    <p class="description">Trabalhamos com equipamentos modernos que garante a qualidade de nossos serviços e a satisfação dos nossos clientes</p>
                </div>
                <div class="icon-box wow fadeInUp" data-wow-delay="0.4s">
                    <div class="icon"><i class="fa fa-bar-chart"></i></div>
                    <h4 class="title"><a href="">Responsabilidade</a></h4>
                    <p class="description">Temos como compromisso atender todas normas e condutas que são atribuídas a qualquer empresa do ramo de dedetização. Utilizamos somente produtos autorizados pela secretaria da saúde e secretaria do meio ambiente.</p>
                </div>
            </div>
            <div class="col-lg-6 background order-lg-2 order-1 wow fadeInRight"></div>
        </div>
    </div>
</section>