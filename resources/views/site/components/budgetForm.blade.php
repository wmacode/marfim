<section id="formBudget">
    <div class="container wow fadeIn">
        <div class="section-header">
            <h3 class="section-title" style="text-transform: none;">FORMULÁRIO DE ORÇAMENTO</h3>
            <p class="section-description"></p>
        </div>
        
        <div class="row row-form-budget">
            <div class="col-lg-7">
                <p>Faça um orçamento. É simples, rapido e pratico. Com apenas alguns cliques você tem a resposta em seu email</p>
            </div>
            <div class="col-lg-5">
                <div id="messageSuccessBudget"></div>
                <div id="messageErrorBudget"></div>
                <form  action="#">
                    {{ csrf_field() }}
                    <div class="form-row">
                        <div class="col-md-12 mb-12">
                            <input type="text" name="name" class="form-control" id="formBudgetName" placeholder="Nome" >
                        </div>
                        <div class="col-md-12 mb-12">
                            <input type="email" name="email" class="form-control" id="formBudgetEmail"  placeholder="Email" >
                        </div>
                        <div class="col-md-12 mb-12">
                            <select name="service" required class="custom-select custom-select-md" id="formBudgetService">
                                <option value="0" selected disabled>Escolha um serviço</option>
                                @foreach($services as $service)
                                    <option value="{{ $service->id }}">{{ $service->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-12">
                            <select
                                name="place"
                                required
                                class="custom-select custom-select-md" 
                                id="formBudgetLocate"
                            >
                                <option value="0" selected disabled>Escolha um local</option>
                                @foreach($places as $place)
                                    <option value="{{ $place->id }}">{{ $place->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div id="qtyRoom" class="col-md-12 mb-12">
                            <input type="number" name="qtyRoom" class="form-control" id="formBudgetRoom" readonly placeholder="">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-12 text-center">
                            <button id="submitBudget" class="btn btn-lg btn-success btn-block btn-custon-form-budget">Enviar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>