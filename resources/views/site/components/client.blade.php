<section id="clients">
    <div class="container wow fadeInUp">
        <div class="section-header">
            <h3 class="section-title">Clientes</h3>
        </div> 
        
        <div class="client">
            @foreach ($clients as $client)
                <div>
                    <img src="{{ $client->filePath }}" alt="clientes">
                </div>
            @endforeach
        </div>
    </div>
</section>