<header id="header">
    <div class="container">
        <div id="logo" class="pull-left">
            <a href="#hero">
                <img src="img/marfim-logo.png" alt="" title="" height="50" /></img>
            </a>
        </div>
        <nav id="nav-menu-container">
            <ul class="nav-menu">
                <li class="menu-active"><a href="#hero">Home</a></li>
                <li><a href="#about">Sobre</a></li>
                <li><a href="#services">Serviços</a></li>
                <li><a href="#formBudget">Orçamento</a></li>
                <li><a href="#clients">Clientes</a></li>
                <li><a href="#contact">Contato</a></li>
            </ul>
        </nav>
        <!-- #nav-menu-container -->
    </div>
</header>