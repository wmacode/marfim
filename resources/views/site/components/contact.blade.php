<section id="contact">
    <div class="container wow fadeInUp">
        <div class="section-header">
            <h3 class="section-title">Contatos</h3>
        </div>
    </div>
    <div class="container wow fadeInUp">
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-4">
                <div class="info">
                    <div>
                        <i class="fa fa-map-marker"></i>
                        <p>Rua Maria Martins 608 <br> Bairro Juliana<br>Belo Horizonte - MG</p>
                    </div>
                    <div>
                        <i class="fa fa-envelope"></i>
                        <p>marfimdedetizadora@contato.com.br</p>
                    </div>
                    <div>
                        <i class="fa fa-phone"></i>
                        <p>(31) 34549859</p>
                    </div>
                </div>
                <div class="social-links">
                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                    <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                    <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                </div>
            </div>
            <div class="col-lg-6 col-md-8">
                
                <div class="form">
                    <div id="messageSuccess"></div>
                    <div id="messageError"></div>
                    <form action="" class="contactForm">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" id="name" placeholder="Seu nome" />
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Seu email" />
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="subject" id="subject" placeholder="Assunto" />
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="message" rows="5" id="message" placeholder="Mensagem"></textarea>
                        </div>
                        <div class="text-center"><buttom id="submit" class="btn btn-lg btn-success btn-block">Enviar Mensagem</button></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>