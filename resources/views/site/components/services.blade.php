<section id="services">
    <div class="container wow fadeIn">
        <div class="section-header">
            <h3 class="section-title">Serviços</h3>
            <p class="section-description"></p>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                <div class="box">
                    <div class="icon"><a href=""><i class="fa fa-bug"></i></a></div>
                    <h4 class="title"><a href="">Desinsetização</a></h4>
                    <p class="description">A desinsetização consiste na eliminação de insetos de modo geral. É feito um trabalho técnico com mapeamento e estudo do local em questão e os resultados sejam de total eliminação dos insetos.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                <div class="box">
                    <div class="icon"><a href=""><i class="fa fa-bug"></i></a></div>
                    <h4 class="title"><a href="">Desratização</a></h4>
                    <p class="description">Controle específico de roedores dos tipos ratazana, rato de telhado, porão e camundongo.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.6s">
                <div class="box">
                    <div class="icon"><a href=""><i class="fa fa-bug"></i></a></div>
                    <h4 class="title"><a href="">Descupinização</a></h4>
                    <p class="description">Consiste no tratamento especializado contra todos tipos de cupins (subterrâneos, cupins de madeira, etc).</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
                <div class="box">
                    <div class="icon"><a href=""><i class="fa fa-bug"></i></a></div>
                    <h4 class="title"><a href="">Controle de pombos</a></h4>
                    <p class="description">Os mecanismos de controle consistem em evitar alimentá-los, não deixar frestas entre telhas, restringir áreas onde os pombos pousam e espantar os animais existentes no local.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.6s">
                <div class="box">
                    <div class="icon"><a href=""><i class="fa fa-bug"></i></a></div>
                    <h4 class="title"><a href="">Higienização de caixas d'agua </a></h4>
                    <p class="description">Oferecemos serviços de limpeza e desinfecção de caixas d'água e reservatórios em residências, condomínios e empresas, com equipes de profissionais especializados, através de metodologias e processos seguros e garantidos.
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
                <div class="box">
                    <div class="icon"><a href=""><i class="fa fa-bug"></i></a></div>
                    <h4 class="title"><a href="">Limpeza de caixa de gordura</a></h4>
                    <p class="description">Retiramos toda sujeira da sua caixa de gordura. Assim evitamos problemas e dor de cabeça para nossas clientes</p>
                </div>
            </div>
        </div>
    </div>
</section>