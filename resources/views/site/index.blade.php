@extends('site.layouts.main') 

@section('content')
    @include('site.components.navbar')
    @include('site.components.loader')
    @include('site.components.hero')
    <main id="main">
        @include('site.components.about')
        @include('site.components.services')
        @include('site.components.callToAction')
        @include('site.components.budgetForm')
        @include('site.components.client')
        @include('site.components.contact')
    </main>
    @include('site.components.footer')
    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
@endsection