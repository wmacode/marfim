const mix = require('laravel-mix');

const pathResourceJsSite = "resources/site/js/";
const pathResourceCssSite = "resources/site/css/";

mix.js([
    pathResourceJsSite + "libs/jquery-migrate.min.js",
    pathResourceJsSite + "libs/bootstrap.bundle.min.js",
    pathResourceJsSite + "libs/easing.min.js",
    pathResourceJsSite + "libs/wow.min.js",
    pathResourceJsSite + "libs/waypoints.min.js",
    pathResourceJsSite + "libs/counterup.min.js",
    pathResourceJsSite + "libs/hoverIntent.js",
    pathResourceJsSite + "libs/superfish.min.js",
    pathResourceJsSite + "libs/slick.js",
    pathResourceJsSite + "budgetForm.js",
    pathResourceJsSite + "contactForm.js",
    pathResourceJsSite + "main.js",
    "node_modules/sweetalert/dist/sweetalert.min.js"
 ], "public/js/site.js" );

mix.styles([
    pathResourceCssSite + "style.css",
    pathResourceCssSite + "template/libs/bootstrap.css",
    pathResourceCssSite + "template/libs/font-awesome.css",
    pathResourceCssSite + "template/libs/animate.css",
    pathResourceCssSite + "template/libs/slick.css",
    pathResourceCssSite + "template/libs/slick-theme.css",
    pathResourceCssSite + "template/template.css",
 ], "public/css/site.css" );


 mix.js('resources/js/app.js', 'public/js/app.js');