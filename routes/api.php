<?php

use Illuminate\Http\Request;

// Route::prefix('auth')->namespace('Auth')->group(function () {
//     Route::post('login', 'AuthController@login');
//     Route::post('refresh', 'AuthController@refresh');
//     Route::middleware('auth:api')->group(function(){
//         Route::post('logout',  'AuthController@logout');
//         Route::post('user',      'AuthController@user');
//     });
// });

//Route::group([
//    'middleware' => 'api',
//    'prefix' => 'auth',
//    'namespace' => 'Auth'
//], function () {
//    Route::post('login', 'AuthController@login');
//    Route::post('logout', 'AuthController@logout');
//    Route::post('refresh', 'AuthController@refresh');
//    Route::post('me', 'AuthController@user');
//});

 Route::middleware('auth:api')->group(function() {
    Route::get('budget', 'BudgetController@findAll');
    Route::get('message', 'MessageController@findAll');

    Route::get('client', 'ClientController@findAll');
    Route::post('client', 'ClientController@save');
    Route::delete('client/{id}', 'ClientController@delete');

    Route::post('user/picture', 'UserController@updateProfilePicture');
    Route::delete('user/picture/{id}', 'UserController@deleteProfilePicture');
    Route::put('user/password', 'UserController@updatePassword');

    Route::get('price', 'PriceController@findAll');
    Route::put('price', 'PriceController@update');

    Route::get('service', 'ServiceController@findAll');
    Route::get('service/{id}', 'ServiceController@find');
    Route::post('service', 'ServiceController@create');
    Route::put('service', 'ServiceController@update');
    Route::delete('service/{id}', 'ServiceController@delete');

    Route::get('place', 'PlaceController@findAll');
    Route::get('place/{id}', 'PlaceController@find');
    Route::post('place', 'PlaceController@create');
    Route::put('place', 'PlaceController@update');
    Route::delete('place/{id}', 'PlaceController@delete');

    Route::get('measure', 'MeasureController@findAll');
    Route::get('measure/{id}', 'MeasureController@find');
    Route::post('measure', 'MeasureController@create');
    Route::put('measure/{id}', 'MeasureController@update');
    Route::delete('measure/{id}', 'MeasureController@delete');

 });
