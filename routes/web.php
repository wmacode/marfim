<?php


// Site Routes...
Route::get('/', 'SiteController@viewIndex')->name('index');
Route::post('/orcamento', 'SiteController@sendBudget')->name('orcamento');
Route::post('/contato', 'SiteController@sendMessage')->name('contato');